import pygame,math

# def move(direction,img,imgx,imgy):
#     img_rect = img.get_rect()
#
#     ydir={'up':1, 'down':-1}
#     xdir={'right':1,'left':-1}
#
#     if direction in ('up','down'):
#         if (imgy >= HEIGHT - img_rect.height or imgy<=0):
#             img = pygame.transform.rotate(img,90)
#             img = pygame.transform.flip(img,True,False)
#         else:
#             imgy += ydir[direction]*2
#
#     if direction in ('right','left'):
#         if
#         imgx += xdir[direction]*2
#             '''
#     if direction == 'right':
#         imgx += 2
#         if imgx >= WIDTH - img_rect.width:
#             direction = 'down'
#             img = pygame.transform.rotate(img,-90)
#     elif direction == 'down':
#         imgy += 2
#         if imgy >= HEIGHT - img_rect.height:
#             direction = 'left'
#             img = pygame.transform.rotate(img,90)
#             img = pygame.transform.flip(img,True,False)
#     elif direction == 'left':
#         imgx -= 2
#         if imgx == 10:
#             direction = 'up'
#             img = pygame.transform.rotate(img,-90)
#     elif direction == 'up':
#         imgy -= 2
#         if imgy == 10:
#             direction = 'right'
#             img = pygame.transform.rotate(img,90)
#             img = pygame.transform.flip(img,True,False)
#             '''
#     img = img.convert()
#     return img,imgx,imgy

def main():
    global BLACK,WHITE,RED,GREEN,BLUE,WIDTH,HEIGHT

    pygame.init()

    WIDTH = 500
    HEIGHT = 500
    BLACK = (0,0,0)
    WHITE = (255,255,255)
    RED = (255,0,0)
    GREEN = (0,255,0)
    BLUE = (0,0,255)

    DISPLAYSURF = pygame.display.set_mode((WIDTH,HEIGHT),0,32)
    pacman_open = pygame.image.load('pacman-open.png')
    pacman_close = pygame.image.load('pacman-closed.png')
    pacman_ghost = pygame.image.load('pacman-ghost.png')
    pacman_logo = pygame.image.load('pacman-logo.png')
    pacman_background = pygame.Surface((pacman_close.get_rect().width,pacman_close.get_rect().height))
    ghost_background = pygame.Surface((pacman_ghost.get_rect().width,pacman_ghost.get_rect().height))

    pygame.display.set_caption('Pacman')
    pygame.display.set_icon(pacman_logo)
    pacman_background.set_colorkey((0,0,0))
    # ghost_background.set_alpha(None)
    # ghost_background.set_colorkey((0,0,0))
    # pacman_ghost.set_alpha(None)
    ghost_background.set_alpha(None)
    ghost_background.set_colorkey((0,0,0))

    pacmanx,pacmany,ghostx,ghosty = 30,30,10,10
    direction = 1
    # rotation = {'right':'left','up':'down'}
    FPS = 20
    speed = 3
    delay = 5
    fpsclock = pygame.time.Clock()

    running = True

    while running:
        DISPLAYSURF.fill(WHITE)

        # print('pacmanx, pacmany : {0},{1}'.format(pacmanx,pacmany))

        event = pygame.event.wait()

        if event.type == pygame.MOUSEMOTION:
            pos = pygame.mouse.get_pos()
            pacmanx += (pos[0] - pacmanx)/delay
            pacmany += (pos[1] - pacmany)/delay
            # if (pos[0] <= pacman_close.get_rect().width - WIDTH and pos[0] > 10) or (pos[1] <= pacman_close.get_rect().height - HEIGHT and pos[1] > 10):
            # pacman_close.get_rect().center = pacmanx,pacmany
        elif event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                running = False

        # if (ghostx <= WIDTH - pacman_ghost.get_rect().width and ghostx > 10) or (ghosty <= HEIGHT - pacman_ghost.get_rect().height and ghosty > 10):
        #     ghostx -= speed
        #     ghosty -= speed

        opposite = pacmany - ghosty
        adjacent = pacmanx - ghostx
        angle = math.atan2(opposite,adjacent)
        if ghostx > pacmanx:
            angle += 100
        dx = speed * math.cos(angle)
        dy = speed * math.sin(angle)
        ghostx += dx
        ghosty += dy

        if pacmanx <= ghostx and pacmany <= ghosty:
            running = False

        # keyspressed = pygame.key.get_pressed()

        # if keyspressed[pygame.K_RIGHT]:
        #     pacman_open,pacmanx,pacmany = move('right',pacman_open,pacmanx,pacmany)
        # elif keyspressed[pygame.K_LEFT]:
        #     pacman_open,pacmanx,pacmany = move('left',pacman_open,pacmanx,pacmany)
        # elif keyspressed[pygame.K_DOWN]:
        #     pacman_open,pacmanx,pacmany = move('down',pacman_open,pacmanx,pacmany)
        # elif keyspressed[pygame.K_UP]:
        #     pacman_open,pacmanx,pacmany = move('up',pacman_open,pacmanx,pacmany)
        #
        # hor = keyspressed[pygame.K_RIGHT] - keyspressed[pygame.K_LEFT]
        # ver = keyspressed[pygame.K_DOWN] - keyspressed[pygame.K_UP]
        #
        # if hor == -hor:
        #     nextrotation = rotation['right']
        # elif ver == -ver:
        #     nextrotation = rotation['up']
        #
        # pacmanx += hor * speed;
        # pacmany += ver * speed;
        #
        # if pacmanx > WIDTH - pacman_open.get_rect().width or pacmanx < 10:
        #     pacmanx -= hor * speed
        # elif pacmany > HEIGHT - pacman_open.get_rect().height or pacmany < 10:
        #     pacmany -= ver * speed
        #
        # if direction == -hor:
        #     pacman_open = pygame.transform.flip(pacman_open,True,False)
        #     direction = hor
        # elif direction == -ver:
        #     pacman_open = pygame.transform.flip(pacman_open,False,True)
        #     direction = ver

        pacman_background.blit(pacman_close,pacman_close.get_rect())
        ghost_background.blit(pacman_ghost,pacman_ghost.get_rect())
        DISPLAYSURF.blit(pacman_background,(pacmanx,pacmany))
        DISPLAYSURF.blit(ghost_background,(ghostx,ghosty))
        pygame.display.update()
        fpsclock.tick(FPS)

if __name__ == '__main__':
    main()
    pygame.quit()
